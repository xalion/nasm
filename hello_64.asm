section .data
    msg  db  'Hello, World!', 10, 0
    len  equ  $ - msg

section .text
global start

start:
    mov rax, 0x2000004      ; Запускаем системный вызов для записи
    mov rbx, 1              ; Указываем поток std out для вывода
    mov rsi, msg            ; Указываем ссылку на сообщение для вывода
    mov rdx, len            ; Указываем длину сообщения для вывода
    syscall                 ; Вызываем прерывание процессора для вывода сообщения

    mov rax, 0x2000001      ; Запусаем системный вызов для завершения программы
    mov rbx, 0
    syscall

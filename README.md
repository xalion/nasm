# Компиляция для Mac OS

`nasm -f macho64 hello_64.asm && ld -macosx_version_min 10.7.0 -lSystem -o 64 hello_64.o && ./64`

# Компиляция для Linux

`nasm -f elf64 hello_64.asm && ld -o 64 hello_64.o`
